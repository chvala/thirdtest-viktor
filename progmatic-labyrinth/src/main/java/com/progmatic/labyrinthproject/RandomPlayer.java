package com.progmatic.labyrinthproject;

import com.progmatic.labyrinthproject.enums.Direction;
import com.progmatic.labyrinthproject.exceptions.CellException;
import com.progmatic.labyrinthproject.exceptions.InvalidMoveException;
import com.progmatic.labyrinthproject.interfaces.Labyrinth;
import com.progmatic.labyrinthproject.interfaces.Player;

import java.util.Random;

public class RandomPlayer implements Player {
    /**
     * Returns the player's next move.
     * This method should not modify the parameter l.
     *
     * @param l
     * @return the player's next move. The returned direction should point to an EMTPY cell within l.
     * If the player has arrived on the END cell, it should return null.
     */
    @Override
    public Direction nextMove(Labyrinth l) throws CellException, InvalidMoveException {
        Random r = new Random();
        return l.possibleMoves().get(r.nextInt((l.possibleMoves().size())));

    }
}
