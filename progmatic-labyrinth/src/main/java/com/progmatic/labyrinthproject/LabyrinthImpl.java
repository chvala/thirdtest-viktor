package com.progmatic.labyrinthproject;

import com.progmatic.labyrinthproject.enums.CellType;
import com.progmatic.labyrinthproject.enums.Direction;
import com.progmatic.labyrinthproject.exceptions.CellException;
import com.progmatic.labyrinthproject.exceptions.InvalidMoveException;
import com.progmatic.labyrinthproject.interfaces.Labyrinth;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author pappgergely
 */
public class LabyrinthImpl implements Labyrinth {
    private int height;
    private int width;
    private CellType[][] theLabyrinth;
    private Coordinate coordinateOfPlayer;

    public LabyrinthImpl() {
    }

    @Override
    public int getWidth() {
        if (theLabyrinth != null) {
            return theLabyrinth.length;
        } else {
            return -1;
        }
    }

    @Override
    public int getHeight() {
        if (theLabyrinth != null) {
            return theLabyrinth[0].length;
        } else {
            return -1;
        }
    }

    @Override
    public CellType getCellType(Coordinate c) throws CellException {
        if (c.getRow() < 0 || c.getCol() < 0 || c.getRow() >= getWidth() || c.getCol() >= getHeight()) {
            throw new CellException(c, "Hibás koordináta, vagy nincs adat");
        } else {
            return theLabyrinth[c.getCol()][c.getRow()];
        }
    }

    @Override
    public void setSize(int width, int height) {
        theLabyrinth = new CellType[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                theLabyrinth[i][j] = CellType.EMPTY;
            }
        }
    }

    @Override
    public void setCellType(Coordinate c, CellType type) throws CellException {
        if (!validCoordinate(c)) {
            throw new CellException(c, "Nem jó koordináta");
        }
        if (type == CellType.START) {
            coordinateOfPlayer = c;
        }
        theLabyrinth[c.getCol()][c.getRow()] = type;
    }


    @Override
    public Coordinate getPlayerPosition() {
        return coordinateOfPlayer;
    }

    private void setPlayerPosition(Coordinate c) {
        coordinateOfPlayer = c;
    }

    @Override
    public boolean hasPlayerFinished() throws CellException {
        return getCellType(coordinateOfPlayer) == CellType.END;

    }

    private boolean validCoordinate(Coordinate c) {
        return (c.getRow() >= 0 || c.getCol() >= 0 || c.getRow() < getWidth() || c.getCol() < getHeight());
    }

    @Override
    public List<Direction> possibleMoves() throws InvalidMoveException {
        List<Direction> answer = new ArrayList<>();
        //változokba kimentem a player koordinátájának X-Y értékét.
        int row = getPlayerPosition().getRow();
        int col = getPlayerPosition().getCol();
        try {
            if (row > 0 && getCellType(new Coordinate(col, row - 1)) != CellType.WALL) {
                answer.add(Direction.NORTH);
            }
            if (col > 0 && getCellType(new Coordinate(col - 1, row)) != CellType.WALL) {
                answer.add(Direction.WEST);
            }
            if (row < getHeight() - 1 && getCellType(new Coordinate(col, row + 1)) != CellType.WALL) {
                answer.add(Direction.SOUTH);
            }
            if (col < getWidth() - 1 && getCellType(new Coordinate(col + 1, row)) != CellType.WALL) {
                answer.add(Direction.EAST);
            }

        } catch (Exception e) {
            throw new InvalidMoveException();
        }
        return answer;
    }

    @Override
    public void movePlayer(Direction direction) throws InvalidMoveException, CellException {
        List<Direction> directions = possibleMoves();
        int row = getPlayerPosition().getRow();
        int col = getPlayerPosition().getCol();
        if (directions.contains(direction)) {
            switch (direction) {
                case NORTH:
                    coordinateOfPlayer = new Coordinate(col, row - 1);
                    break;
                case SOUTH:
                    coordinateOfPlayer = new Coordinate(col, row + 1);
                    break;
                case WEST:
                    coordinateOfPlayer = new Coordinate(col - 1, row);
                    break;
                case EAST:
                    coordinateOfPlayer = new Coordinate(col + 1, row);
                    break;
                default:
                    throw new InvalidMoveException();

            }
        }

    }

    @Override
    public void loadLabyrinthFile(String fileName) {
        try {
            Scanner sc = new Scanner(new File(fileName));
            int width = Integer.parseInt(sc.nextLine());
            int height = Integer.parseInt(sc.nextLine());
            theLabyrinth = new CellType[width][height];
            setSize(width, height);
            for (int hh = 0; hh < height; hh++) {
                String line = sc.nextLine();
                for (int ww = 0; ww < width; ww++) {
                    switch (line.charAt(ww)) {
                        case 'W':
                            theLabyrinth[ww][hh] = CellType.WALL;
                            break;
                        case 'E':
                            theLabyrinth[ww][hh] = CellType.END;
                            break;
                        case 'S':
                            theLabyrinth[ww][hh] = CellType.START;
                            setPlayerPosition(new Coordinate(ww, hh));
                            break;
                        default:
                            theLabyrinth[ww][hh] = CellType.EMPTY;
                            break;
                    }
                }
            }
        } catch (FileNotFoundException | NumberFormatException ex) {
            System.out.println(ex.toString());
        }
    }

}
